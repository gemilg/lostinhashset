package pl.edu.pwr.lostinhashset.service.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import pl.edu.pwr.lostinhashset.entity.User;
import pl.edu.pwr.lostinhashset.repository.UserRepository;
import pl.edu.pwr.lostinhashset.service.UserService;

public class DefaultUserService implements UserService {
	
	private UserRepository userRepository;

	@Override
	public List<User> getUsersSortedByName() {
		
		List<User> users = getUserRepository().getUsers();
		users.sort(new Comparator<User>(){

			@Override
			public int compare(User o1, User o2) {
				// TODO Auto-generated method stub
				return o1.getName().compareTo(o2.getName());
			}});
		return users;
	}

	@Override
	public List<User> getUsersSortedByPoints() {

		List<User> users = getUserRepository().getUsers();
		users.sort(new Comparator<User>(){

			@Override                      
			public int compare(User o1, User o2) {
				// TODO Auto-generated method stub
				if(o1.getPoints() < o2.getPoints()) return -1;
				else if(o1.getPoints() > o2.getPoints()) return 1;
				else return 0;
			}});
		return users;
	}

	public UserRepository getUserRepository() {
		return userRepository;
	}

	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

}
